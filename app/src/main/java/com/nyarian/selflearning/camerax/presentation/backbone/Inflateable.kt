package com.nyarian.selflearning.camerax.presentation.backbone

interface Inflateable {

    fun inflate(layoutId: Int)

}