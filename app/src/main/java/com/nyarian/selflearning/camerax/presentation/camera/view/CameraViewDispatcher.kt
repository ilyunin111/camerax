package com.nyarian.selflearning.camerax.presentation.camera.view

interface CameraViewDispatcher {

    fun onCapture()

    fun onSwitchFacing()

    fun onGalleryClick()

}