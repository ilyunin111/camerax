package com.nyarian.selflearning.camerax.presentation.camera.firmware

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.nyarian.selflearning.camerax.R

class CameraActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.camera_navigation_container)
    }

}
