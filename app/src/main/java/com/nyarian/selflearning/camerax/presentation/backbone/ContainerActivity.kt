package com.nyarian.selflearning.camerax.presentation.backbone

import android.view.View
import androidx.appcompat.app.AppCompatActivity

abstract class ContainerActivity : AppCompatActivity(), Inflateable, Traversable {

    override fun inflate(layoutId: Int) {
        setContentView(layoutId)
    }

    override fun find(id: Int): View = findViewById(id)

}