package com.nyarian.selflearning.camerax.presentation.camera.view

import android.view.View
import com.nyarian.selflearning.camerax.R
import com.nyarian.selflearning.camerax.presentation.backbone.onClick
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.camera_controls.*

class CameraControlsView(override val containerView: View, dispatcher: CameraViewDispatcher) : LayoutContainer {

    init {
        camera_capture_button.onClick(dispatcher::onCapture)
        camera_switch_button.onClick(dispatcher::onSwitchFacing)
        photo_view_button.onClick(dispatcher::onGalleryClick)
    }

    companion object {
        const val layout = R.layout.camera_controls
    }

}