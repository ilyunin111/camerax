package com.nyarian.selflearning.camerax.presentation.backbone

import android.view.View

fun View.onClick(callback: () -> Unit) {
    setOnClickListener { callback() }
}
