package com.nyarian.selflearning.camerax.presentation.camera.view

import android.util.DisplayMetrics
import android.view.View
import androidx.camera.core.Preview
import androidx.camera.core.PreviewConfig
import androidx.constraintlayout.widget.ConstraintLayout
import com.nyarian.selflearning.camerax.R
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.camera.*

class CameraView(
    private val dispatcher: CameraViewDispatcher,
    override val containerView: View
) : LayoutContainer {

    lateinit var preview: Preview

    val windowRotation get() = preview_texture.display.rotation

    fun updateControls() {
        camera_container.findViewById<ConstraintLayout>(R.id.camera_ui_container)?.let(camera_container::removeView)
        CameraControlsView(View.inflate(containerView.context, R.layout.camera_controls, camera_container), dispatcher)
    }

    fun getRealMetrics(metrics: DisplayMetrics) {
        preview_texture.display.getRealMetrics(metrics)
    }

    fun createPreview(config: PreviewConfig) {
        preview = AutoFitPreviewBuilder.build(config, preview_texture)
    }

    fun onLayout(callback: () -> Unit) {
        preview_texture.post(callback)
    }

    companion object {
        const val layout = R.layout.camera
    }

}
