package com.nyarian.selflearning.camerax.presentation.camera.firmware

import android.content.Context
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Rational
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.camera.core.CameraX
import androidx.camera.core.PreviewConfig
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation.findNavController
import com.nyarian.selflearning.camerax.R
import com.nyarian.selflearning.camerax.presentation.camera.firmware.CameraFragmentDirections.actionCameraToPermissions
import com.nyarian.selflearning.camerax.presentation.camera.view.CameraView
import com.nyarian.selflearning.camerax.presentation.camera.view.CameraViewDispatcher

class CameraFragment : Fragment(), CameraViewDispatcher {
    private var cameraView: CameraView? = null

    override fun onCapture() {
        //TODO
    }

    override fun onSwitchFacing() {
        //TODO
    }

    override fun onGalleryClick() {
        //TODO
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(CameraView.layout, container, false)
            .also { cameraView = CameraView(this, it) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cameraView!!.onLayout { cameraView?.apply(CameraView::updateControls)?.apply { adjustPreview(this) } }
    }

    private fun adjustPreview(cameraView: CameraView) {
        val metrics = DisplayMetrics().also(cameraView::getRealMetrics)
        val screenAspectRatio = Rational(metrics.widthPixels, metrics.heightPixels)
        PreviewConfig.Builder().apply {
            setLensFacing(CameraX.LensFacing.BACK)
            setTargetAspectRatio(screenAspectRatio)
            setTargetRotation(cameraView.windowRotation)
        }.build().apply(cameraView::createPreview)
        CameraX.bindToLifecycle(this, cameraView.preview)
    }

    override fun onResume() {
        super.onResume()
        if (!PermissionsFragment.hasPermissions(requireContext())) {
            findNavController(requireActivity(), R.id.fragment_container).navigate(actionCameraToPermissions())
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        cameraView = null
    }

}