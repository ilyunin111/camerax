package com.nyarian.selflearning.camerax.presentation.backbone

import android.view.View

interface Traversable {

    fun find(id: Int): View

}